const IMIE = "Krzysztof";
const NAZWISKO = "Kowalski";
let a = 23;
let b = 45;
let c = 67;

let prawda = true;
let falsz = false;

let user1 = {
  imie: "krzysztof",
  nazwisko: "Kowalski",
  email: "krzysztof@as.pl",
  wiek: 23,
  aktywny: true,
  adres: {
    ulica: "Kwiatowa",
    numer: 23,
    kod: "12-345",
    miasto: "Warszawa",
  },
  zaloguj: function () {
    console.log("zalogowano");
  },
};
let user2 = {
  imie: "Anna",
  nazwisko: "Nowak",
  email: "",
};

user1.imie = "Krzysztof22222";

let osoby = [user1, user2];

// Definicja funkcji
function dodajOsobe(imie, nazwisko, email) {
  let osoba = {
    imie: imie,
    nazwisko: nazwisko,
    email: email,
  };
  osoby.push(osoba);
}

// Wywołanie funkcji
dodajOsobe("Jan", "Kowalski", "asdf@asfd.pl");
dodajOsobe("Janina", "Kowalska", "asdf@asfd.pl");
dodajOsobe("Piotr", "Kowalski", "asdf@asfd.pl");
console.log("Hello");
Array.from(10000).forEach((i) => {
  dodajOsobe("Jan", "Kowalski", "email@asdf.pl");
});

console.log(osoby);
